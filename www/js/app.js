// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'starter.services'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider,$ionicConfigProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js

  $ionicConfigProvider.tabs.position('bottom'); // other values: top
  $stateProvider

  // setup an abstract state for the tabs directive
    .state('tab', {
    url: '/tab',
    abstract: true,
    templateUrl: 'templates/tabs.html',
    
  })

  .state('login', {
    url: '/login',
    templateUrl: 'templates/login.html',
    controller: 'LoginCtrl'
  })

  // Each tab has its own nav history stack:

  .state('tab.rutas', {
    url: '/rutas',
    views: {
      'tab-rutas': {
        templateUrl: 'templates/tab-rutas.html',
        controller: 'RutasCtrl'
      }
    }
  })
  .state('tab.ruta-detalle', {
    url: '/rutas/:rutaId',
    views: {
      'tab-rutas': {
        templateUrl: 'templates/ruta-detalle.html',
        controller: 'RutaDetalleCtrl'
      }
    }
  })


  .state('tab.surtido', {
    url: '/surtido',
    views: {
      'tab-surtido': {
        templateUrl: 'templates/tab-surtido.html',
        controller: 'SurtidoCtrl'
      }
    }
  })
  .state('tab.surtido-detalle', {
    url: '/surtido/:surtidoId',
    views: {
      'tab-surtido': {
        templateUrl: 'templates/surtido-detalle.html',
        controller: 'SurtidoDetalleCtrl'
      }
    }
  })

  .state('tab.clientes', {
    url: '/clientes',
    views: {
      'tab-clientes': {
        templateUrl: 'templates/tab-clientes.html',
        controller: 'ClientesCtrl'
      }
    }
  })

  .state('tab.corte', {
    url: '/corte',
    views: {
      'tab-corte': {
        templateUrl: 'templates/tab-corte.html',
        controller: 'CorteCtrl'
      }
    }
  })

  .state('tab.cuenta', {
    url: '/cuenta',
    views: {
      'tab-cuenta': {
        templateUrl: 'templates/tab-cuenta.html',
        controller: 'CuentaCtrl'
      }
    }
  })

  .state('tab.chats', {
      url: '/chats',
      views: {
        'tab-chats': {
          templateUrl: 'templates/tab-chats.html',
          controller: 'ChatsCtrl'
        }
      }
    })
    .state('tab.chat-detail', {
      url: '/chats/:chatId',
      views: {
        'tab-chats': {
          templateUrl: 'templates/chat-detail.html',
          controller: 'ChatDetailCtrl'
        }
      }
    });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/login');

});

