var app = angular.module('starter.controllers', [])

app.controller('ChatsCtrl', function($scope, Chats) {
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  $scope.chats = Chats.all();
  $scope.remove = function(chat) {
    Chats.remove(chat);
  };
})

app.controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
  $scope.chat = Chats.get($stateParams.chatId);
})

app.controller('LoginCtrl', function($scope,$http,Server,$state,DataSession) {

  $scope.cMensaje ="Login";
  $scope.lstVehiculos = [];
  

  $scope.ValidarLogin = function(cUsuario,cPassword,bLogin,nIdVehiculo)
  {
    //alert(bLogin);
    if(bLogin == true)
    {
      //alert(bLogin);
        $scope.SeleccionarVehiculo(nIdVehiculo);
    }
    else
    {
      $scope.Login(cUsuario,cPassword);
    }

  }

  $scope.Login = function(cUsuario,cPassword)
  {
    $http({
      headers: {
         'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      },
      method: "POST",
      url: Server.data.url + "Login",
      data: $.param({
        cLogin: cUsuario,
        cPassword: cPassword
      })
    }).success(function(response) {
        console.log('response',response);

        if(!response.bError)
        {
          DataSession.data = response;
          $scope.ObtenerVehiculos(response.nIdSucursal);
          //$state.go("tab.rutas");
        }
        else
        {
          alert("Usuario o Password incorrectos");
        }
      
        
       
      }).error(function(data, status, headers, config) {
        console.log(data);
        console.log(status);
        console.log(headers);
        console.log(config);
        
      });

  }


  $scope.ObtenerVehiculos = function(nIdSucursal)
  {
    $http({
      headers: {
         'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      },
      method: "POST",
      url: Server.data.url + "ObtenerVehiculos",
      data: $.param({
        nIdSucursal: nIdSucursal,
      })
    }).success(function(response) {
        console.log('response',response);

        if(!response.bError)
        {
          $scope.lstVehiculos = response.lstVehiculos;
          //$state.go("tab.rutas");
        }
        else
        {
          alert("Usuario o Password incorrectos");
        }
      
        
       
      }).error(function(data, status, headers, config) {
       
      });

  }

  $scope.SeleccionarVehiculo = function(nIdVehiculo)
  {
    //alert(DataSession.data.lstRutas[0].nIdRuta);
    //console.log("DataSession",DataSession);
    $http({
      headers: {
         'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      },
      method: "POST",
      url: Server.data.url + "SeleccionarVehiculo",
      data: $.param({
        nIdRuta: DataSession.data.lstRutas[0].nIdRuta,
        nIdVehiculo: nIdVehiculo
      })
    }).success(function(response) {

        if(!response.bError)
        {
          $state.go("tab.rutas");
          //alert("exito");
        }
        else
        {
          alert("Usuario o Password incorrectos");
        }
      
        
       
      }).error(function(data, status, headers, config) {
       
      });
  }

})

app.controller('RutasCtrl', function($scope,Server,$http,DataSession) {
  $scope.cMensaje ="";
  $scope.lstRutas = [
    {
      nIdRuta:1,
      cNombreCliente:"Tacos Sonora",
      cDescripcion: "Entrega de producto",
      cFoto:"img/ben.png"
    },
    {
      nIdRuta:2,
      cNombreCliente:"Abarrote Roger",
      cDescripcion: "Cobranza",
      cFoto:"img/ben.png"
    },
    {
      nIdRuta:3,
      cNombreCliente:"Super MZ",
      cDescripcion: "Entrega de producto",
      cFoto:"img/ben.png"
    },
    {
      nIdRuta:4,
      cNombreCliente:"Don Jose",
      cDescripcion: "Entrega de producto",
      cFoto:"img/ben.png"
    },
    {
      nIdRuta:5,
      cNombreCliente:"Taqueria don chuy",
      cDescripcion: "Entrega de producto",
      cFoto:"img/ben.png"
    },

  ];

  $scope.lstRutas = DataSession.data.lstRutas[0].lstClientes;


})

app.controller('RutaDetalleCtrl', function($scope, $stateParams, DataSession,$ionicModal) {
  //$scope.chat = Chats.get($stateParams.rutaId);

 
  $scope.lstPresentaciones = [];
  
  
  for(var i in DataSession.data.lstSurtido)
  {
    for(var j in DataSession.data.lstSurtido[i].lstPresentaciones)
    {  
      DataSession.data.lstSurtido[i].lstPresentaciones[j].cDescripcion = DataSession.data.lstSurtido[i].cDescripcion + " " +  DataSession.data.lstSurtido[i].lstPresentaciones[j].cDescripcion
      $scope.lstPresentaciones.push(DataSession.data.lstSurtido[i].lstPresentaciones[j]);
    
    }
    //DataSession.data.lstSurtido[i].cPresentaciones =  cPresentaciones;
  }

    $scope.ModalVenta;
    $ionicModal.fromTemplateUrl('templates/Modal/ModalSurtidoRuta.html', {
      scope: $scope
    }).then(function(modal) {
      $scope.ModalVenta = modal;
    });


    
  

  

 

  for(var i in DataSession.data.lstRutas[0].lstClientes)
  {
    if(DataSession.data.lstRutas[0].lstClientes[i].nIdRutaDetalle == $stateParams.rutaId)
    {
      $scope.Cliente = DataSession.data.lstRutas[0].lstClientes[i];
    }
  }

  // 

   console.log("Cliente",$scope.Cliente);


   $scope.AbrirModalVenta = function()
   {
     $scope.ModalVenta.show();
     console.log("Presentaciones",DataSession.data);
   }


})

app.controller('SurtidoCtrl', function($scope,$http,DataSession,Server) {
  $scope.cMensaje ="SurtidoCtrl";
  $scope.lstSurtido = [];

  $scope.ObtenerSurtido = function()
  {

    $http({
      headers: {
         'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      },
      method: "POST",
      url: Server.data.url + "ObtenerSurtido",
      data: $.param({
        nIdRuta: DataSession.data.lstRutas[0].nIdRuta,

      })
    }).success(function(response) {
        console.log('response',response);

        if(!response.bError)
        {
          DataSession.data.lstSurtido = response.lstProuctos;
          console.log("lstSurtido",  DataSession.data.lstSurtido);
          

          for(var i in DataSession.data.lstSurtido)
          {
            var cPresentaciones = "";
            for(var j in DataSession.data.lstSurtido[i].lstPresentaciones)
            {
              cPresentaciones += " | " + DataSession.data.lstSurtido[i].lstPresentaciones[j].cDescripcion;
            }
            DataSession.data.lstSurtido[i].cPresentaciones =  cPresentaciones;
          }

          $scope.lstSurtido = DataSession.data.lstSurtido;
          

        }
        else
        {
          alert("Usuario algo salio mal");
        }
      
        
       
      }).error(function(data, status, headers, config) {
       
        
      });
  }

  // $scope.lstSurtido = [
  //   {
  //     nIdProducto:1,
  //     cProducto:"Jamaica",
  //     cDescripcion: "200ml. | 500ml. | 1L.",
  //   },
  //   {
  //     nIdProducto:2,
  //     cProducto:"Limon",
  //     cDescripcion: "200ml. | 500ml. | 1L.",
  //   },
  //   {
  //     nIdProducto:3,
  //     cProducto:"Horchata",
  //     cDescripcion: "200ml. | 500ml. | 1L.",
  //   },
  //   {
  //     nIdProducto:4,
  //     cProducto:"Tamarindo",
  //     cDescripcion: "200ml. | 500ml. | 1L.",
  //   },
  //   {
  //     nIdProducto:5,
  //     cProducto:"Piña",
  //     cDescripcion: "200ml. | 500ml. | 1L.",
  //   },
    

  // ];
})
app.controller('SurtidoDetalleCtrl', function($scope, $stateParams,DataSession) {

  $scope.Producto = DataSession.data.lstSurtido[$stateParams.surtidoId];

  console.log("Producto",$scope.Producto);
// for(var i in DataSession.data.lstProuctos)
// {
//   if(DataSession.data.lstProuctos[i].cDescripcion == $stateParams.surtidoId)
//   {
//     $scope.Producto = DataSession.data.lstProuctos[i];
//     console.log("Producto", $scope.Producto);
//   }
// }

  

  //$stateParams.surtidoId
  // $scope.Producto = {
  //   cProducto:  "",
  //   nIdProducto:0,
  //   lstPresentaciones:[
  //     {
  //       nIdPresentacion:1,
  //       cDescripcion:"200ml.",
  //       nPiezas:34
  //     },
  //     {
  //       nIdPresentacion:2,
  //       cDescripcion:"500ml.",
  //       nPiezas:20
  //     },
  //     {
  //       nIdPresentacion:3,
  //       cDescripcion:"1l.",
  //       nPiezas:10
  //     }
  //   ]
  // };
  // $scope.Producto.nIdProducto = parseInt($stateParams.surtidoId);
  // switch(parseInt($stateParams.surtidoId))
  // {
  //   case 1: $scope.Producto.cProducto = "Jamaica"; break;
  //   case 2: $scope.Producto.cProducto = "Limon"; break;
  //   case 3: $scope.Producto.cProducto = "Horchata"; break;
  //   case 4: $scope.Producto.cProducto = "Tamarindo"; break;
  //   case 5: $scope.Producto.cProducto = "Piña"; break;
  // }
  
  
})

app.controller('ClientesCtrl', function($scope,$ionicModal) {

  $scope.modalCliente;
  $ionicModal.fromTemplateUrl('templates/Modal/ModalClientes.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modalCliente = modal;
  });


  $scope.AbrirModal = function()
  {
    $scope.modalCliente.show();

  }
  $scope.CerrarModal = function()
  {
    //$scope.modalCliente.close();

  }

})

app.controller('CorteCtrl', function($scope) {
  
})

app.controller('CuentaCtrl', function($scope) {
  
})




;
